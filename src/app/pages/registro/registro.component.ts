import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from '../../models/usuario.model';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  usuario:UsuarioModel
  recordarme = false
  constructor(private authService:AuthService, private router:Router) {
    this.usuario = new UsuarioModel()
  }

  ngOnInit(): void {
  }

  // respuesta
  // "idToken": "[ID_TOKEN]",
  // "email": "[user@example.com]",
  // "refreshToken": "[REFRESH_TOKEN]",
  // "expiresIn": "3600",
  // "localId": "tRcfmLH7..."
  onSubmit(form:NgForm){
    if (form.invalid) {
      return;
    }
    Swal.fire("","Espere porfavor", "info")
    Swal.showLoading()
    this.authService.nuevoUsuario(this.usuario)
    .subscribe( resp => {
      console.log(resp);
      Swal.close()
      if (this.recordarme) {
        localStorage.setItem("email", this.usuario.email)
      }
      this.router.navigateByUrl('/home')
    }, (err) => {
      console.log(err.error.error.message);
      Swal.fire("",err.error.error.message, "error")
    })
  }

}
